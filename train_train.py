import os
import sys
import numpy as np
import torch
from torch.optim import Adam

# Idea: predict the appropriate step size (one scalar for now).
# What is the loss for the predictor ? We don't know the best step size...
# Can we now after the fact if the step was too large or too thin ?
# The procedure should still be faster overall

# Or we train a network to predict the next value given the history of values and a step size and use it to
# It can be a fairly simple network so

# It seems training must be done beforehand on a simple function

tensor_type = torch.FloatTensor

def f(x):
    return x + 2*x*2 + x**4 + 1


theta = torch.from_numpy(np.random.normal(size=1)).type(tensor_type)
theta.requires_grad_(True)

optimizer = Adam([theta], lr=1e-2)

iterations = 1000

for i in range(iterations):
    optimizer.zero_grad()
    loss = f(theta)
    print(theta, loss)
    loss.backward()
    optimizer.step()
