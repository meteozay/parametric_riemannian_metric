from euclidean_exponential import EuclideanExponential
#from parametric_calculator.fourier_exponential import FourierExponential
from logistic_exponential import LogisticExponential
from parametric_exponential import ParametricExponential

"""
Reads a dictionary of parameters, and returns the corresponding exponential object.
"""


class ExponentialFactory:
    def __init__(self, manifold_type, dimension, manifold_parameters=None):
        self.manifold_type = manifold_type
        self.manifold_parameters = manifold_parameters
        self.dimension = dimension

        if self.manifold_type == 'parametric':
            assert self.manifold_parameters is not None, 'Please provide parameters for parametric exponential'

    def set_manifold_type(self, manifold_type):
        self.manifold_type = manifold_type

    def set_parameters(self, manifold_parameters):
        self.manifold_parameters = manifold_parameters

    def create(self):
        """
        Returns an exponential for a manifold of a given type, using the parameters
        """
        if self.manifold_type == 'parametric':
            out = ParametricExponential(dimension=self.dimension)
            out.width = self.manifold_parameters['width']
            out.number_of_interpolation_points = self.manifold_parameters['interpolation_points'].size()[0]
            out.interpolation_points_torch = self.manifold_parameters['interpolation_points']
            out.set_parameters(self.manifold_parameters['interpolation_values'])
            out.dimension = out.interpolation_points_torch.size()[1]
            return out

        if self.manifold_type == 'fourier':
            assert False, 'Fourier geodesic no longer supported'
            # return FourierExponential()

        if self.manifold_type == 'logistic':
            out = LogisticExponential(dimension=self.dimension)
            return out

        if self.manifold_type == 'deep': # what the heck is this ?
            out = EuclideanExponential(dimension=self.dimension)
            return out

        if self.manifold_type == 'euclidean':
            out = EuclideanExponential(dimension=self.dimension)
            return out

        raise ValueError("Unrecognized manifold type in exponential factory")

