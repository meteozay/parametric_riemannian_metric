import os
import torch
from longitudinal_scalar_dataset import LongitudinalScalarDataset
from parametric_model import ParametricModel
import numpy as np
from estimate_parametric_model import estimate_parametric_model
from torch.utils.data import DataLoader

from sklearn.model_selection import KFold
import argparse
import pandas as pd


def save_dataset_info(dataset, model, output_dir, prefix='train'):
    dataloader = DataLoader(dataset)
    ids, sources, xis, taus = model.encode_all(dataloader)
    np.savetxt(os.path.join(output_dir, prefix + '_sources.txt'), sources)
    np.savetxt(os.path.join(output_dir, prefix + '_ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, prefix + '_xis.txt'), xis)
    np.savetxt(os.path.join(output_dir, prefix + '_taus.txt'), taus)


def extract_id_from_name(name):
    return float(name[-4:])


def launch_simulation(output_dir, train_dataset, test_dataset, nb_sources, randomize_nb_observations=False, l=10.):
    print('Output directory', output_dir)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
#    test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

    parametric_model = ParametricModel(
        width=0.8,
        data_dim=4,
        nb_sources=nb_sources,
        encoder_hidden_dim=64,
        labels=['memory', 'language', 'praxis', 'concentration'],
        type=tensor_type
    )

    def call_back(model):
        save_dataset_info(train_dataset, model, output_dir, prefix='train')
        #save_dataset_info(test_dataset, model, 'test', output_dir)

    estimate_parametric_model(parametric_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=16, save_every_n_iters=5,
                                        call_back=call_back, lr_decay=0.99,
                                        randomize_nb_observations=randomize_nb_observations, l=l,
                                        estimate_noise=False)

# Parsing some command line arguments
# parser = argparse.ArgumentParser()
# parser.add_argument("-fold", "-fold", default=0)
# parser.add_argument("-l","-l", default=10.)
# args = parser.parse_args()
# fold_number = int(args.fold)

fold_number = 0
l = 1.

print('Value of l used {}'.format(l))

# Loading the data
df = pd.read_csv('../../ipmi2017/data/data_adas/df.csv')
print(df.head())
ids = df.values[:, 0]
ids = [extract_id_from_name(elt) for elt in ids]
values = df.values[:, 2:6]
times = df.values[:, 1]


nb_sources = 1

labels = {}
for rid in ids:
    labels[rid] = 0

distinct_rids = np.array(list(set(ids)))


kf = KFold(n_splits=10, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

tensor_type = torch.DoubleTensor

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    if fold == fold_number:
        train_ids_unique = distinct_rids[train_index]
        test_ids_unique = distinct_rids[test_index]

        train_ids, train_values, train_times = [], [], []
        test_ids, test_values, test_times = [], [], []
        for (rid, value, time) in zip(ids, values, times):
            if rid in train_ids_unique:
                train_ids.append(rid)
                train_values.append(value)
                train_times.append(time)

            else:
                assert rid in test_ids_unique
                test_ids.append(rid)
                test_values.append(value)
                test_times.append(time)

        output_dir = '../output/output_test'

        train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times, type=tensor_type)
        #test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
        #                                         ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)

        launch_simulation(output_dir, train_dataset, None, nb_sources, randomize_nb_observations=True, l=1.)
